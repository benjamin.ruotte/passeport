package com.passport.application.appjava;

import com.recognition.software.jdeskew.ImageDeskew;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

public class Mask {

    //Définition d'un angle minimum pour le test d'alignement
    private static final double MINIMUM_DESKEW_THRESHOLD = 0.7;

    private Mask() {}

    public static JLabel run(JLabel source) throws Exception {

        JOptionPane popup = new JOptionPane();
        popup.setName("test");
        JLabel frame = new JLabel();

        //Appel de la fonction createCoordinateList
        //Permet de créer une liste à deux dimensions contenant toutes les coordonnées nécessaire à la création de masque -> (y-, y+, x-, y+)
        ArrayList<ArrayList<Integer>> areas = createCoordinateList();

        //
        Image image = ((ImageIcon)source.getIcon()).getImage();

        //Création d'un objet BufferedImage depuis l'image uploadé afin d'en extraire ses données
        BufferedImage biImage = (BufferedImage) image;

        //Conversion de l'image en matrice
        Mat matImage = Tool.bufferedImageToMat(biImage);

        //Appel de la fonction createMask afin de créer un masque pour isoler les champs à tester
        //Retourne une matrice correspondant au masque
        Mat mask = createMask(matImage);

        //Appel de la fonction getSub permettant de découper le masque pour en extraire les matrices correspondant à chaque champ
        ArrayList<Mat> matChamps = getSub(mask, areas);

        //Appel de la fonction matToBuff
        //Permet de convertir chaque matrice correspondant aux champs en BufferedImage
        //Renvoi une liste d'objet Buffobj(bi)
        ArrayList<Buffobj> listeBuffobj = matToBuff(matChamps);

        //Appel de la fonction Alignement qui vérifie que le texte de chaque champs n'a pas d'inclinaison
        //Renvoi la liste d'objet Buffobj(bi, angle) qui correspond aux champs incliné
        ArrayList<Buffobj>testA = alignement(listeBuffobj);

        //Appel de la fonction Decalage qui vérifie si un des champs est décalé par rapport aux autres (sur l'absisse x)
        //Renvoi la liste d'objet Buffobj(bi, x) qui correspond aux champs décallé
        ArrayList<Buffobj>testD = decalage(listeBuffobj);

        //Teste si la liste issue du test d'Alignement est vide
        //Si la liste contient des élements alors le résultat du test est négatif
        if(!testA.isEmpty()){
            JOptionPane.showMessageDialog(popup, "Test D'Alignement échoué, Document non valide !", "Echec", JOptionPane.ERROR_MESSAGE);

            //Affiche le premier champ en erreur
            return Tool.getJLabel(frame, Tool.bufferedImageToMat(testA.get(0).getBufferedImage()));
        }

        //Teste si la liste issue du test de Décalage est vide
        //Si la liste contient des élements alors le résultat du test est négatif
        if(!testD.isEmpty()){
            JOptionPane.showMessageDialog(popup, "Test de Décalage échoué, Document non valide !", "Echec", JOptionPane.ERROR_MESSAGE);

            //Affiche le premier champ en erreur
            return Tool.getJLabel(frame, Tool.bufferedImageToMat(testD.get(0).getBufferedImage()));
        }
        JOptionPane.showMessageDialog(popup, "Tous les tests ont été réalisés avec succes. Le document est valide", "Réussite", JOptionPane.ERROR_MESSAGE);

        //Si les résultats des tests sont positifs alors on renvoi
        return Tool.getJLabel(frame, mask);
    }

    //Génère une liste de liste de int contenant les coordonnées des champs
    static ArrayList<ArrayList<Integer>> createCoordinateList()
    {
        ArrayList<ArrayList<Integer>> champs = new ArrayList<>();
        ArrayList<Integer> ca =  new ArrayList<>();
        ca.add(239);
        ca.add(299);
        ca.add(1123);
        ca.add(1395);
        ArrayList<Integer> name = new ArrayList<>();
        name.add(328);
        name.add(383);
        name.add(560);
        name.add(1055);
        ArrayList<Integer> prenom = new ArrayList<>();
        prenom.add(410);
        prenom.add(467);
        prenom.add(560);
        prenom.add(1055);
        ArrayList<Integer> sex = new ArrayList<>();
        sex.add(500);
        sex.add(547);
        sex.add(560);
        sex.add(652);
        ArrayList<Integer> nationalite = new ArrayList<>();
        nationalite.add(500);
        nationalite.add(547);
        nationalite.add(740);
        nationalite.add(1055);
        ArrayList<Integer> dissue = new ArrayList<>();
        dissue.add(664);
        dissue.add(718);
        dissue.add(560);
        dissue.add(1055);
        ArrayList<Integer> dexpiry = new ArrayList<>();
        dexpiry.add(747);
        dexpiry.add(801);
        dexpiry.add(560);
        dexpiry.add(1055);
        ArrayList<Integer> birth = new ArrayList<>();
        birth.add(580);
        birth.add(634);
        birth.add(560);
        birth.add(1055);
        champs.add(ca);
        champs.add(name);
        champs.add(prenom);
        champs.add(sex);
        champs.add(nationalite);
        champs.add(dissue);
        champs.add(dexpiry);
        champs.add(birth);

        return champs;
    }

    //Dessine le masque sur l'image
    public static Mat createMask(Mat image)
    {

        //Création d'une matrice tampon ayant les dimensions de l'image
        Mat dst = new Mat(image.rows(),image.cols(), CvType.CV_8UC1);

        //Conversion de l'image en matrice
        image = Tool.convertMat(image);

        //Application de deux filtres afin de tranformer l'image en noir et blanc et faire ressortir le texte
        //les filtres sont appliqués sur la matrice tampon
        Imgproc.adaptiveThreshold(image, dst, 125, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 11, 12);

        //Création d'une matrice aux mêmes dimensions qui servira de masque
        Mat mask = new Mat(image.rows(), image.cols(), CvType.CV_8UC1, Scalar.all(0));

        //Applique des rectangles autour des champs selon des coordonnées bien précises calculer en dehors de l'application
        //Code CA
        Imgproc.rectangle(mask, new Point(1123, 239), new Point(1395,299), new Scalar(255, 255, 255), -1, 8, 0);
        //Nom
        Imgproc.rectangle(mask, new Point(560, 328), new Point(1055,383), new Scalar(255, 255, 255), -1, 8, 0);
        //FirstName
        Imgproc.rectangle(mask, new Point(560, 410), new Point(1055,467), new Scalar(255, 255, 255), -1, 8, 0);
        //Sex
        Imgproc.rectangle(mask, new Point(560, 500), new Point(652,547), new Scalar(255, 255, 255), -1, 8, 0);
        //Nationality
        Imgproc.rectangle(mask, new Point(740, 500), new Point(1055,547), new Scalar(255, 255, 255), -1, 8, 0);
        //Date of issue
        Imgproc.rectangle(mask, new Point(560, 664), new Point(1055,718), new Scalar(255, 255, 255), -1, 8, 0);
        //Date of expiry
        Imgproc.rectangle(mask, new Point(560, 747), new Point(1055,801), new Scalar(255, 255, 255), -1, 8, 0);
        //Code
        Imgproc.rectangle(mask, new Point(561.5, 341), new Point(685.5,357), new Scalar(255, 255, 255), -1, 8, 0);
        //Date of birth
        Imgproc.rectangle(mask, new Point(560, 580), new Point(1055,634), new Scalar(255, 255, 255), -1, 8, 0);

        //Création d'une nouvelle matrice
        Mat cropped = new Mat();

        //Applique les filtres et créer une matrice uniquement avec les champs à tester
        dst.copyTo(cropped, mask);
        return cropped;
    }

    //Fonction permettant de retourner une liste de matrice correspondant à chaque champs défini dans le masque depuis la liste des coordonnées
    public static ArrayList<Mat> getSub(Mat src, ArrayList<ArrayList<Integer>> listeMat) {

        //Création d'une liste de matrice
        ArrayList<Mat> subMats = new ArrayList<>();

        //Boucle la liste des coordonnées de chaque champs
        for(ArrayList<Integer> matChamp : listeMat) {

            //Création de matrices selon les coordonnées x,y de chaque champs
            Mat subMat = src.submat(matChamp.get(0), matChamp.get(1), matChamp.get(2), matChamp.get(3));

            //Ajout de matrice dans la liste créer auparevent
            subMats.add(subMat);
        }
        return subMats;
    }

    //Fonction permettant de convertir des matrices en BufferedImage
    private static ArrayList<Buffobj> matToBuff(ArrayList<Mat> listMat) throws Exception {

        //Création d'e matrice de Bytes
        MatOfByte mob = new MatOfByte();

        //Création d'une liste d'objets Buffobj
        ArrayList<Buffobj> listBi = new ArrayList<>();

        //Parcours la liste de matrice en entrée
        for(Mat mat:listMat){

            //Conversion des matrices en image puis en Bufferedimage
            Imgcodecs.imencode(".jpg", mat, mob);
            byte[] ba = mob.toArray();
            BufferedImage bi = ImageIO.read(new ByteArrayInputStream(ba));

            //Création d'un objet Buffobj(bi) contenant la Bufferedimage créee plus haut
            Buffobj bo = new Buffobj(bi);

            //Ajout de l'objet à la liste
            listBi.add(bo);
        }
        return listBi;
    }

    //Fonction pour tester si un champs est incliné
    public static ArrayList<Buffobj> alignement(ArrayList<Buffobj>listeBuffobj){

        //Création d'une liste d'objets Buffobj
        ArrayList<Buffobj>listeInvalide = new ArrayList<>();

        //Parcours la liste des objets Buffobj en entrée
        for(Buffobj bo:listeBuffobj){

            //Création d'un objet ImageDeskew qui permet de calculer l'inclinaison
            //ImageDeskew est une classe de Tess4j
            ImageDeskew id = new ImageDeskew(bo.getBufferedImage());

            //Retourne l'angle d'inclinaison
            double angle = id.getSkewAngle();

            //Test si l'angle est supérieur ou inférieur à la variable défini au début
            if ((angle > MINIMUM_DESKEW_THRESHOLD || angle < -(MINIMUM_DESKEW_THRESHOLD))){

                //Si l'angle est différent alors on créer un objet Buffobj(bi, angle) avec l'image et la valeur de l'angle
                Buffobj bofalse = new Buffobj(bo.getBufferedImage(), angle);

                //On ajoute cette objet à la liste invalide
                listeInvalide.add(bofalse);
            }
        }
        //Renvoie la liste contenant tous les champs qui ne passent pas le test
        return listeInvalide;
    }

    //Fonction pour tester le décalage entre la premiere lettre et le bord gauche du masque
    //avec en entrée la liste des champs à tester
    public static ArrayList<Buffobj> decalage(ArrayList<Buffobj> listeBuffobj)
    {

        //Création d'une liste d'objet Buffobj
        ArrayList<Buffobj>listBo = new ArrayList<>();

        //Boucle la liste en entrée
        for(Buffobj bo:listeBuffobj){

            //Variables contenant les dimensions du champs à tester
            final int xMin = bo.getBufferedImage().getMinX();
            final int yMin = bo.getBufferedImage().getMinY();
            final int xMax = xMin + bo.getBufferedImage().getWidth();
            final int yMax = yMin + bo.getBufferedImage().getHeight();

            //Parcours les pixels du champs à tester pour trouver les pixels noirs correspondant au texte
            int x = xMax;
            for(int i = xMin;i<xMax;i++){
                for (int j = yMin;j<yMax;j++){
                    int pixel = bo.getBufferedImage().getRGB(i, j);

                    //Teste la couleur du pixel

                    //si le pixel est noir alors il s'agit de texte
                    if((pixel & 0x00FFFFFF) == 0 && x>i){
                        //Permet de trouver la coordonnée x du premier pixel noir
                            x = i;
                    }
                }
            }

            //Création d'un objet Buffobj(bi, x) contenant le champ et la coordonnée x du premier pixel noir
            Buffobj bo2 = new Buffobj(bo.getBufferedImage(), x);

            //Ajoute l'objet crée dans une liste
            listBo.add(bo2);
        }

        //Création d'une liste d'objets Buffobj
        ArrayList<Buffobj>listeInvalide = new ArrayList<>();

        //Boucle la liste des Buffobj de la liste biliste
        //la variable a contient la coordonnée x du premier pixel noir du premier champs à tester
        int xPos = listBo.get(0).getX();
        for(int i = 1; i < listBo.size(); i++){

            //Si la coordonnée x du premier pixel noir est inférieure ou supérieure à a
            //alors ce champ est décallé. Il est donc ajouter à la liste invalide
            if(listBo.get(i).getX() > (xPos + 1)) {
                listeInvalide.add(listBo.get(i));
            }
            if(listBo.get(i).getX() < (xPos - 1)){
                listeInvalide.add(listBo.get(i));
            }
        }

        //Renvoie une liste d'objet Buffobj contenant les champs décaler et donc non valide
        return listeInvalide;
    }
}
