package com.passport.application.appjava;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Analyse implements ActionListener {
    private final JFrame window;

    public Analyse (JFrame window)
    {
        this.window = window;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        try {
            buttonActionPerformed();
        } catch (IOException ex) {
            Logger.getLogger(PrintImage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void buttonActionPerformed() throws IOException {
        if (window.getContentPane().getComponentCount() > 0) {
            JLabel source = (JLabel) window.getContentPane().getComponent(0);
            window.remove(window.getContentPane().getComponent(0));
            try {
                window.add(Mask.run(source));
            } catch (Exception e) {
                e.printStackTrace();
            }
            window.repaint();
            window.revalidate();
            window.setVisible(true);
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Message d'erreur : aucun fichier choisie", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }
}