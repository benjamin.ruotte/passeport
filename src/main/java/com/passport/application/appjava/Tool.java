package com.passport.application.appjava;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Tool {
    private Tool() {}

    static JLabel getJLabel(JLabel frame, Mat mask) throws IOException {
        BufferedImage bufImage = matToBufferedImage(mask);
        frame.setIcon(new ImageIcon(bufImage));
        frame.setName("Image");
        return frame;
    }

    static BufferedImage matToBufferedImage(Mat matrix)throws IOException {
        MatOfByte mob=new MatOfByte();
        Imgcodecs.imencode(".bmp", matrix, mob);
        return ImageIO.read(new ByteArrayInputStream(mob.toArray()));
    }

    static Mat bufferedImageToMat(BufferedImage image) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "bmp", byteArrayOutputStream);
        byteArrayOutputStream.flush();
        return Imgcodecs.imdecode(new MatOfByte(byteArrayOutputStream.toByteArray()), Imgcodecs.IMREAD_UNCHANGED);
    }

    static Mat convertMat(Mat src)
    {
        Mat out = new Mat(src.rows(),src.cols(), CvType.CV_8UC1);
        Imgproc.cvtColor(src,out,Imgproc.COLOR_RGB2GRAY);
        return out;
    }
}
