package com.passport.application.appjava;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Close implements ActionListener {
    JFrame window;

    public Close (JFrame window)
    {
        this.window = window;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        this.window.dispose();
    }
}
