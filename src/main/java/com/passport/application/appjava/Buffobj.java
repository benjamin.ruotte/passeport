package com.passport.application.appjava;

import java.awt.image.BufferedImage;

public class Buffobj {
    private BufferedImage bi;
    private double angle;
    private int x;

    public Buffobj(){
    }
    public Buffobj(BufferedImage pbi, double pangle){
        bi = pbi;
        angle = pangle;
    }
    public Buffobj(BufferedImage pbi, int px){
        bi = pbi;
        x = px;
    }
    public Buffobj(BufferedImage pbi){
        bi = pbi;
    }

    public BufferedImage getBufferedImage() {
        return bi;
    }
    public double getAngle(){
        return angle;
    }
    public int getX(){
        return x;
    }
}