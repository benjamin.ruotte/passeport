package com.passport.application.appjava;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Open implements ActionListener {
    private final JFileChooser fileChooser;
    private final JFrame window;

    public Open (JFrame window)
    {
        fileChooser = new JFileChooser();
        fileChooser.setName("OpenChooser");
        FileTypeFilter filterPng = new FileTypeFilter(".png", "png File");
        FileTypeFilter filterJpeg = new FileTypeFilter(".jpeg", "jpeg File");
        fileChooser.addChoosableFileFilter(filterPng);
        fileChooser.addChoosableFileFilter(filterJpeg);
        this.window = window;
    }

    @Override
    public void actionPerformed(ActionEvent evt){buttonActionPerformed();
    }

    private void buttonActionPerformed() {
            if (fileChooser.showOpenDialog(window) == JFileChooser.APPROVE_OPTION) {
                String selectedFile = fileChooser.getSelectedFile().getAbsolutePath();
                if (window.getContentPane().getComponentCount() > 0)
                    window.getContentPane().remove(0);
                window.add(PrintImage.run(selectedFile));
                window.repaint();
                window.revalidate();
                window.setVisible(true);
            }
    }
}