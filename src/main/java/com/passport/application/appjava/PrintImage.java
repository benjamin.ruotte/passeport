package com.passport.application.appjava;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import javax.swing.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrintImage {
    private PrintImage() {
    }

    public static JLabel run(String filePath) {
        JLabel frame = new JLabel();
       try {
           Mat image = Imgcodecs.imread(filePath);
           return Tool.getJLabel(frame, image);
       }
       catch (Exception ex)
       {
           Logger.getLogger(PrintImage.class.getName()).log(Level.SEVERE, null, ex);
       }
       return frame;
    }
}