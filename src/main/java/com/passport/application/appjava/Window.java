package com.passport.application.appjava;

import org.opencv.core.Core;

import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class Window extends JFrame{

    public Window() {
        this.setTitle("Main window");
        this.setSize(400, 400);
        this.setLocationRelativeTo(null);
        this.setJMenuBar(this.menu());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public JMenuBar menu()
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;

        //Create the menu bar.
        menuBar = new JMenuBar();

        //Build the first menu.
        menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_A);
        menu.getAccessibleContext().setAccessibleDescription("Control the window");
        menuBar.add(menu);

        //a group of JMenuItems
        menuItem = new JMenuItem("Close", KeyEvent.VK_T);
        menuItem.setName("Close");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.ALT_DOWN_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription("Allow you to close the window");
        menuItem.addActionListener(new Close(this));
        menu.add(menuItem);

        //Build second menu in the menu bar.
        menu = new JMenu("Edition");
        menu.setMnemonic(KeyEvent.VK_N);
        menu.getAccessibleContext().setAccessibleDescription("Control the content");
        menuBar.add(menu);

        //a group of JMenuItems
        menuItem = new JMenuItem("Open", KeyEvent.VK_T);
        menuItem.setName("Open");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.ALT_DOWN_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription("Allow you to select and open a file");
        menuItem.addActionListener(new Open(this));
        menu.add(menuItem);

        //a group of JMenuItems
        menuItem = new JMenuItem("Analyse", KeyEvent.VK_T);
        menuItem.setName("Analyse");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, InputEvent.ALT_DOWN_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription("Start the analyse of the picture");
        menuItem.addActionListener(new Analyse(this));
        menu.add(menuItem);
        return menuBar;
    }
}
