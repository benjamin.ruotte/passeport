package com.passport.application.appjava;
import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.finder.JOptionPaneFinder;
import org.assertj.swing.fixture.FrameFixture;
import static java.awt.event.KeyEvent.*;

import org.assertj.swing.fixture.JOptionPaneFixture;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;


public class AppJavaTest {

    private FrameFixture window;

    @BeforeClass
    public static void setUpOnce() {
        FailOnThreadViolationRepaintManager.install();
    }

    @Before
    public void setUp() {
        Window frame = GuiActionRunner.execute(Window::new);
        window = new FrameFixture(frame);
    }
    @Test
    public void AppJavaTestOpen() {
        File file = new File("document/image/dutch_card.jpg");
        window.menuItem("Open").click();
        window.fileChooser("OpenChooser").setCurrentDirectory(file);
        window.fileChooser("OpenChooser").selectFile(file);
        window.fileChooser("OpenChooser").approve();
        window.menuItem("Analyse").click();
        JOptionPaneFixture frame = JOptionPaneFinder.findOptionPane().using(window.robot());
        frame.pressKey(VK_ENTER);
        window.menuItem("Close").click();
    }
}
